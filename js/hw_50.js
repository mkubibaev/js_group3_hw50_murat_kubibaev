class Machine {
	constructor() {
		this.enabled = false;
	}

    turnOn() {
	    if (!this.enabled) {
            this.enabled = true;
            console.log('turn on');
        }
    }

    turnOff() {
	    if (this.enabled) {
            this.enabled = false;
            console.log('turn off');
        }
    }
}

class HomeAppliance extends Machine {
    constructor() {
        super();
        this.plugged = false;
    }

    plugIn() {
        if (!this.plugged) {
            this.plugged = true;
            console.log('plug in');
        }
    };

    plugOff() {
        if (this.plugged) {
            this.plugged = false;
            console.log('plug off');
        }
    };
}

class WashingMachine extends HomeAppliance {
    constructor() {
        super();
    }

    run() {
        if(this.plugged) {
            console.log('run');
        }
    }
}

class LightSource extends HomeAppliance {
    constructor() {
        super();
    }

    setLevel(level) {
        this.level = level;
        console.log(`light level = ${this.level}`);
    }
}

class AutoVehicle extends Machine {
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
        console.log(`current position: x = ${this.x}, y = ${this.y}`);
    }
}

class Car extends AutoVehicle {
    constructor() {
        super();
        this.speed = 10;
    }

    setSpeed(speed) {
        this.speed = speed;
        console.log(`speed = ${this.speed}`);
    }

    run(x, y) {
        console.log(`target position: x = ${x}, y = ${y}`);
        let moveID = setInterval(() => {

            /* использую строчную функцию,
            так как она берет контекст того места, где вызвана, т.е. контекст run() */

            let newX = this.x + this.speed;
            let newY = this.y + this.speed;

            if (newX >= x) {
                newX = x;
            }
            if (newY >= y) {
                newY = y;
            }

            this.setPosition(newX, newY);

            if (newX === x && newY === y) {
                clearInterval(moveID);
                console.log('finish');
            }
        }, 1000)
    }
}